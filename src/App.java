public class App {
    public static void main(String[] args) throws Exception {
        // Khởi tạo hai đối tượng animal1, animal2
        Animal animal1 = new Animal("Duck");
        Animal animal2 = new Animal("Cow");
        // in ra console
        System.out.println(animal1.toString());
        System.out.println(animal2.toString());

        // khởi tạo hai đối tượng mammal1 và mammal2
        Mammal mammal1 = new Mammal("Sheep");
        Mammal mammal2 = new Mammal("Goat");
        // in ra console
        System.out.println(mammal1.toString());
        System.out.println(mammal2.toString());

        // khởi tạo 2 đối tượng Cat1, Cat2
        Cat cat1 = new Cat("Kitty");
        Cat cat2 = new Cat("Tom");
        // in ra console
        System.out.println(cat1.toString());
        System.out.println(cat2.toString());
        // in ra tiếng kêu của đối tượng
        cat1.greets();
        cat2.greets();

        // khởi tạo 2 đối tượng Dog1, Dog2
        Dog dog1 = new Dog("Doggy");
        Dog dog2 = new Dog("Lucky");
        // in ra console
        System.out.println(dog1.toString());
        System.out.println(dog2.toString());
        // in ra tiếng kêu của đối tượng
        dog1.greets();
        dog2.greets();
        // in ra tiếng kêu của đối tượng dog1 với đối tượng dog2
        dog1.greets(dog2);

    }
}