public class Cat extends Mammal {
    public Cat (String name){
        super(name);
    }
    
    public void greets(){
        System.out.println("Meow");
    }

    //in ra console log
    @Override
    public String toString(){
        return String.format("Cat [Mammal[Animal [Name = %s]]]", name);
    }
}
