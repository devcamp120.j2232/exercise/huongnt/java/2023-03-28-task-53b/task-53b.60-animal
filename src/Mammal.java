public class Mammal extends Animal{
    public Mammal(String name){
        super(name);
    }
    
    //in ra console
    @Override
    public String toString(){
        return String.format("Mammal [Animal [Name = %s]]", name);
    }
}
